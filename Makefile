all:
	@echo Only need to install. Nothing to build

install: /usr/local/man/man1/mirror-pkg.1 /usr/local/bin/mirror-pkg

/usr/local/man/man1/mirror-pkg.1: mirror-pkg.1
	@install -d /usr/local/man/man1
	@install -Cv -m 444 mirror-pkg.1 /usr/local/man/man1

/usr/local/bin/mirror-pkg: mirror-pkg.pl
	@install -d /usr/local/bin
	@install -Cv -m 555 mirror-pkg.pl /usr/local/bin/mirror-pkg
