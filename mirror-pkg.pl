#!/usr/bin/env perl

# mirror the freebsd pkg repo. they say they don't support it, but I'm
#
# I'm running this with this command in the crontab:
# /usr/local/bin/mirror-pkg -repo /crackles/repos/FreeBSD/freebsd:10:x86:64

use strict;
use File::Basename;
use Getopt::Long;
use JSON::XS;

my $ME = basename($0);

# unbuffered output
$|=1;

# set totals early in case Ctrl-T called
my $totalAdded = 0;
my $totalUpdated = 0;
my $totalFetched = 0;
my $totalPkgs = 0;
my $totalRemotePkgs = 0;
my $totalSkipped = 0;
my $totalThere = 0;
my $totalPruned = 0;
my $totalFailed = 0;
my $needToUpdate = 0;
my $updatePkgsite = 0;

sub printTotals {
  my $SAVE = $SIG{INFO};
  $SIG{INFO} = undef;
  print <<EOM;

Total packages skipped:   $totalSkipped
Total packages added:     $totalAdded
Total packages updated:   $totalUpdated
Total packages good asis: $totalThere
                          __________
Total packages examined:  $totalPkgs

EOM
  if ($totalPruned) {
    print "Total packages pruned:    $totalPruned\n";
  }
  if ($totalFailed) {
    print "Total failed fetches:     $totalFailed\n";
  }
  $SIG{INFO} = $SAVE;
}

# handle SIGINFO and print total
$SIG{INFO} = sub {
  if ($totalRemotePkgs > 0) {
    printf <<INFO, ($totalPkgs/$totalRemotePkgs)*100;

Received INFO signal

Processed $totalPkgs out of $totalRemotePkgs (%5.2f%) packages
INFO
    printTotals()
  }
  else {
    print "Haven't queried remote repo yet. Try later\n";
  }
};

# set path for execution
$ENV{'PATH'} = "/usr/local/bin:/usr/bin:$ENV{'PATH'}";

# default values
my $workdir = undef;
my $urlRoot = "http://pkg.freebsd.org";
my $repoDir;
my $fetchLimit = 0;
my $dotLimit = 100;
my $onlyPattern = undef;
my $noReplace = undef;
my $force = undef;
my $removeWork = 1;
my $prune = undef;
my $subDir = "latest";

GetOptions(
  "prune"                => \$prune,
  "dots=i"               => \$dotLimit,
  "force"                => \$force,
  "count|limit|number=i" => \$fetchLimit,
  "noreplace"            => \$noReplace,
  "only=s"               => \$onlyPattern,
  "repo=s"               => \$repoDir,
  "workdir=s"            => \$workdir,
);
# must have $repoDir, exit it now
if (! $repoDir) {
  print <<EOM;

$ME: ERROR: I must have the local repo to build/update.

Please supply a -repo <value> option.

EOM
  exit 1;
}
# $repoDir must be of form "freebsd:<version>:<arch>" where
# version might be 10, arch might be x86:64. It might need
# to by symlinked to something like FreeBSD:10:amd64. The
# freebsd.org site uses the lowercase name.
if ($repoDir !~ m?/(freebsd:\d+:x86:\d\d)/*(.*)?) {
  print "Expecting a repo directory something like:\n";
  print "  /xx/yy/freebsd:13:x86:64/latest\n";
  exit 1;
}
my $abiDir = $1;
if ($2) {
  $subDir = $2;
}
else {
  $repoDir .= "/$subDir";
}
my $allDir = "$repoDir/All";
if ( ! -d $allDir ) {
  `mkdir -p $allDir`;
}

# find out what tool to use for "fetching" curl, wget, and fetch are
# check in that order
my $FETCHCMD = undef;
foreach my $tool (qw(curl wget fetch)) {
  chomp(my $path = `which $tool 2>/dev/null`);
  if ( -x $path) {
    if ($tool eq "curl") {
      $FETCHCMD = "$path -sfR -o";
      last;
    }
    elsif ($tool eq "wget") {
      $FETCHCMD = "$path -q -O";
      last;
    }
    else {
      $FETCHCMD = "$path -o";
      last;
    }
  }
}
if (! "$FETCHCMD") {
  print "Can't find any fetch tool (curl, wget or fetch)\n";
  exit 1;
}

# create a working dir
if ($workdir) {
  $removeWork = 0;
}
else {
  $workdir = "/tmp/mirror-pkg.$$";
}
if ( ! -d "$workdir") {
  `mkdir -p $workdir`;
}
chdir $workdir;

# by default we will use /latest. This can be overriden
# with the -subdir <dir> option. Currently /quarterly is the
# only other feasible value. Don't include the '/' when
# specifying the value.
#
# get current version of packagesite.txz for comparison to
# our current file structure. fetch any files that are missing.
#
my $urlDir = "$urlRoot/$abiDir/$subDir";
print "Fetching metadata from remote repo ...\n";
`${FETCHCMD} packagesite.txz $urlDir/packagesite.txz`;
# check for file here
if ( ! -f "packagesite.txz") {
  print "Failed to fetch $urlDir/packagesite.txz\n";
  exit 1;
}
if ( ! $force || ! -f "$repoDir/packagesite.txz" ) {
  chomp(my $urlmd5 = `md5 -q packagesite.txz`);
  chomp(my $repomd5 = `md5 -q $repoDir/packagesite.txz 2>/dev/null`);
  if ( $urlmd5 eq $repomd5 ) {
    print "Meta data is not different, exiting\n";
    exit 1;
  }
}
$updatePkgsite = 1;
`tar xzf packagesite.txz packagesite.yaml`;
# get all packages in remote's packagesite
chomp(my @remoteJSON = `cat packagesite.yaml`);
# .yaml is just an array of JSON things, make it JSON
print "Convert .yaml to JSON and decode\n";
my $json = '{"pkgs":[' . join(',', @remoteJSON) . ']}';
utf8::encode($json);
my $allPkgs = decode_json($json);

# seems like the suffix changed from .txz to .pkg in FreeBSD 12
# so determine that here and use it from here on out
chomp(my $bsdversion = `uname -r | cut -d'.' -f1`);
my $suffix = (${bsdversion} > 11) ? 'pkg' : 'txz';

# get all packages in our repo
print "Creating a list of all packages in our repo ...\n";
chomp(my @localPkgs = `find $allDir -name '*.${suffix}' | xargs basename`);

print <<EOH;
Installing missing packages from:
  $urlDir
into: $repoDir

(one '.' is $dotLimit packages skipped)
EOH

my %urlPkgs;
$totalRemotePkgs = scalar @{$allPkgs->{pkgs}};
foreach my $ref (@{$allPkgs->{pkgs}}) {
  $totalPkgs++;
  print '.' if ($totalPkgs % $dotLimit == 0);
  my $pkg = "$ref->{name}-$ref->{version}.${suffix}";
  $urlPkgs{$pkg} = 1;
  my $repoPkg = "$repoDir/All/$pkg";
  my $urlPkg = "$urlDir/All/$pkg";
  # just skip certain packages
  #   -o <pattern> means we will skip any pattern that doesn't
  # match /<pattern>/
  # if -o not specified, do NOT install any language specific stuff
  if ($onlyPattern && $pkg !~ /$onlyPattern/) {
    $totalSkipped++;
    next;
  }
  # skip locally updated packages. The first instance of this was
  # when gcc was built with a bootstrap option that broke gcc builds
  # on FreeBSD 11.1-RELEASE. We rebuilt the package with the bootstrap
  # option disabled. We renamed the original package with a .orig on the
  # end. If we see a file "$repoPkg" . orig, just move on to the next pkg.
  if ( -f "${repoPkg}.orig") {
    $totalSkipped++;
    $needToUpdate = 1;
    next;
  }
  my $why;
  # if the file is there, check the filesize against data
  # if same, we're ok, else get it
  if ( -f "$repoPkg") {
    my @pkgStat = stat($repoPkg);
    if ($pkgStat[7] == $ref->{pkgsize}) {
      $totalThere++;
      next;
    }
    $why = 'U';
    $totalUpdated++;
    $needToUpdate = 1;
  }
  else {
    $why = 'A';
    $totalAdded++;
    $needToUpdate = 1;
  }
  print "\nFetching($why) $pkg ";
  `${FETCHCMD} $repoPkg $urlPkg`;
  # check for file here
  if ( ! -f $repoPkg) {
    print "Failed to fetch $urlPkg\n";
    $totalFailed++;
    next;
  }
  $totalFetched++;
  if ($fetchLimit && $totalFetched >= $fetchLimit) {
    print "\n\nStopped fetching due to -limit $fetchLimit option\n";
    $noReplace = 1;
    $updatePkgsite = 0;
    last;
  }
}

# if prune given, prune out old pkgs (not in downloaded packagesite file
if ($needToUpdate && $prune) {
  chdir $allDir;
  print "\nPruning old packages from local repo\n";
  foreach my $pkg (@localPkgs) {
    if (! defined $urlPkgs{$pkg}) {
      `rm $pkg`;
       $totalPruned++;
    }
  }
}

# print totals
printTotals();

#
# now if we got this far, we did have a difference in the packagesite.txz
# file so let's generate our meta files
if ($needToUpdate && ! $noReplace) {
  chdir $repoDir;
  print "Updating the pkg* files in Latest/\n";
  `mkdir -p Latest`;
  my $pkg = glob("All/pkg-[1-9]*.${suffix}");
  my $devel = glob("All/pkg-devel-[1-9]*.${suffix}");
  system("ln -fs ../$pkg Latest/pkg.${suffix}") if ($pkg);
  system("ln -fs ../$devel Latest/pkg-devel.${suffix}") if ($devel);
  print "Recreating the meta files\n";
  system("pkg repo .")
}

exit 0;

# always remove workdir at end. Unless one was specified
END {
  if ($removeWork) {
    chdir;
    `rm -rf $workdir`;
  }
}
