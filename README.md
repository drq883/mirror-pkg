# mirror-pkg

The FreeBSD mirror howto page says 

> Due to very high requirements of bandwidth, storage and adminstration the FreeBSD Project has decided not to allow public mirrors of packages.

Bummer!

I started thinking about this and know that:

* Their pkg repos contain a packagesite.txz file
which describes the entire repo

* I can use fetch to grab any file.

With this two things and the JSON perl module, I can mirror the repo without relying on rsync.

The Makefile will install mirror-pkg into /usr/local/bin so a simple crontab entry can run the command:

/usr/local/bin/mirror-pkg -repo /repos/FreeBSD/freebsd:10:x86:64 
 
I run this once a day at 6am and send the output to a /var/log file.
 
Files:
 
* Makefile      - to install the script and man page
* mirror-pkg.pl - perl script to keep a mirror in sync with the SOURCE
* mirror-pkg.1  - man page for this
* mirror-pkg.cron.sample - sample cron file to mirror FreeBSD:10 and log activity
* mirror-pkg.conf.sample - sample newsyslog config to roll log 

 
